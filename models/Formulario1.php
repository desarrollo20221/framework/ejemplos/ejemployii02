<?php

namespace app\models;

use yii\base\Model;


class Formulario1 extends Model{
    public $numero1;
    public $numero2;
    
    public function rules() {
        return [
            [["numero1", "numero2"],'required'],
            [["numero1", "numero2"], 'integer', "max" => 100],
        ];
    }
    
    public function Suma(){
        return $this->numero1+$this->numero2;
    }
    
    public function Producto() {
        return $this->numero1*$this->numero2;
    }
    
    public function resultados() {
        return [
            "suma" => $this->Suma(),
            "producto" => $this->Producto(),
        ];
    }
}
