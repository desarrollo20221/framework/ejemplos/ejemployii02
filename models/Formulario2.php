<?php

namespace app\models;

use yii\base\Model;

class Formulario2 extends Model{
    public $texto;
    
    public function rules() {
        return [
            ["texto", 'required']
        ];               
    }
    
    public function caracteres() {
        
        return strlen($this->texto);
    }
    
    // Relacion entre el titulo de la propiedad (label) y el nombre de la propiedad
    public function attributeLabels(): array {
        
        return [
            "texto" => "Introduce un texto",
        ];
    }


    
}
