<?php
    use yii\helpers\Html;
?>

<h1><?= $titulo ?></h1>
<div>
    <p><?= $contenido ?></p>
</div>

<div>
<?php
    echo Html::tag("p", $contenido);
?>
</div>

